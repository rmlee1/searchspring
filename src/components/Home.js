import React from 'react'
import searchResults from '../json'
import styled from 'styled-components'
import Item from './Item'
import {Icon} from 'semantic-ui-react'
const ItemContainer = styled.div`
width:100%;
display:flex;
flex-wrap:wrap;
flex-direction:wrap;
justify-content:space-around;
`
const HomeContainer = styled.div`
display:flex;
flex-direction:column;

`
const Header = styled.div`
display:flex;
justify-content:flex-end;
padding: 10px 0;
background:#131921;
`
const CartDiv = styled.div`
padding:0 15px 0 0;

color:orange;
display:flex;
align-items:center;
p{font-size:1.7rem;}
`
function Home() {
    
    return (
        <>
    
        <HomeContainer>
            <Header>
        <CartDiv>
        <p style={{margin:'auto 3px'}}>0</p>    
        <Icon size='big' color='grey' name='cart' />
        <p style={{color:'white',fontSize:'1rem',marginTop:'10px'}}>Cart</p>
        </CartDiv>  
        </Header>
      <ItemContainer>
        {searchResults.results.map(res  =>(
            
            <Item header={res.title} description={res.description} image={res.thumbnailImageUrl} price={res.price}  msrp={res.msrp} />
        ))}
      </ItemContainer>
      </HomeContainer>
      </>
    )
}
export default Home