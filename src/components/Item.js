import React from 'react'
import {Card,Icon,Button,Image} from 'semantic-ui-react'

export default function Item(props) {

    function trimDescription(){
        return props.description.substring(0,props.description.indexOf('\u2022'))
    }

    return (
        <Card style={{margin:"10px"}}>
        <Image src={props.image} wrapped ui={false} />
        <Card.Content>
          <Card.Header>{props.header}</Card.Header>
          <Card.Description>
              {trimDescription}
          </Card.Description>
          <Card.Meta>
            <span> <Icon color="white"name='dollar sign'/>{props.price}</span>
            <span><Icon color="white"name='dollar sign'/>MSRP {props.msrp}</span>
          </Card.Meta>
      
        </Card.Content>
        <Card.Content extra style={{margin:'0 auto'}}>
          <Button color='primary'>
            <Icon color="white"name='add to cart' />
            Add To Cart
          </Button>
        </Card.Content>
      </Card>
    )
}
