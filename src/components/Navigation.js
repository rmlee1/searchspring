import React,{useState} from 'react'
import styled from 'styled-components'
import {Button,Input,Select,Search,Header,Segment,Sidebar} from 'semantic-ui-react'
import searchResults from '../json'

const StyledNav =styled.div `

width:10%;
min-width:220px;
height:100vh;
min-height:100%;
display:flex;

justify-content:space-between;
background:#131921;
@media (max-width: 768px) {
    flex-direction: column;
  }
`
const ButtonContainer = styled.div`
display:flex;
flex-direction: column;
margin:80px 0 0;

`
export default function Navigation() {

    const [results,setResults] = useState(searchResults)
    const [value,setValue] = useState('')
    const  searchChange = (e) => {

    }

    
    return (
        <Sidebar.Pushable>
        <Sidebar
        animation='overlay'
        
        
        >
        <StyledNav>
            <ButtonContainer >
             <Search
              input={{ icon: 'search', iconPosition: 'left' }}
              onSearchChange={searchChange}
              value={value}
            />
            </ButtonContainer>
        </StyledNav>
        </Sidebar>
        </Sidebar.Pushable>
    )
}


// <Input type='text' size='mini' placeholder='Search' action>
//              <input/>
//              {/* <Select compact options={filters} defaultValue={'any'} /> */}
//              <Button size='mini'color='primary'type='submit'>Search</Button>
//              </Input>