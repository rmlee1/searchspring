import './App.css';
import {Route} from 'react-router-dom'
import Search from './components/Search'
import Home from './components/Home'
import Navigation from './components/Navigation'
function App() {
  return (
    <div style={{display:'flex'}}>
      <Navigation />
     <Home />
    </div>
  );
}

export default App;
